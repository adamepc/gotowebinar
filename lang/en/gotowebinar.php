<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * English strings for gotowebinar
 *
 * You can have a rather longer description of the file as well,
 * if you like, and it can span multiple lines.
 *
 * @package    mod_gotowebinar
 * @copyright  2016 Your Name <your@email.address>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['modulename'] = 'gotowebinar';
$string['modulenameplural'] = 'gotowebinars';
$string['modulename_help'] = 'Use the gotowebinar module for... | The gotowebinar module allows...';
$string['gotowebinar:addinstance'] = 'Add a new gotowebinar';
$string['gotowebinar:submit'] = 'Submit gotowebinar';
$string['gotowebinar:view'] = 'View gotowebinar';
$string['gotowebinarfieldset'] = 'Custom example fieldset';
$string['gotowebinarname'] = 'gotowebinar name';
$string['gotowebinarname_help'] = 'This is the content of the help tooltip associated with the gotowebinarname field. Markdown syntax is supported.';
$string['gotowebinar'] = 'gotowebinar';
$string['pluginadministration'] = 'gotowebinar administration';
$string['pluginname'] = 'gotowebinar';
